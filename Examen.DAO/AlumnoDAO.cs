﻿using Examen.BUSS;
using Examen.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examen.DAO
{
    public class AlumnoDAO
    {
        readonly FileHelper fileHelper = new FileHelper();

        public Estatus GuardarAlumno(Alumno alumno)
        {
            Estatus estatus = new Estatus
            {
                IsSuccess = false,
            };

            try
            {
                fileHelper.AlmacenarRegistro(alumno);
                estatus.IsSuccess = true;
                estatus.Message = "Almacenamiento de archivo exitoso";
            }catch(Exception e)
            {
                estatus.Message = "Error al Almacenar el archivo";
            }

            return estatus;
        }
    }
}
