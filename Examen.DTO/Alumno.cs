﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Examen.Models
{
    public class Alumno
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Display(Name = "Nombre *")]
        [Required(ErrorMessage = "El campo Nombre es requerido")]
        [RegularExpression("[A-Za-zÁÉÍÓÚÑáéíóúñ\\.\\,\\s]{3,250}", ErrorMessage = "Escriba solo letras.")]
        public string Nombre { get; set; }

        [Display(Name = "Apellido Paterno *")]
        [Required(ErrorMessage = "El campo Apellido Paterno es requerido")]
        [RegularExpression("[A-Za-zÁÉÍÓÚÑáéíóúñ\\.\\,\\s]{3,250}", ErrorMessage = "Escriba solo letras.")]
        public string ApellidoPaterno { get; set; }

        [Display(Name = "Apellido Materno *")]
        [Required(ErrorMessage = "El campo Apellido Materno es requerido")]
        [RegularExpression("[A-Za-zÁÉÍÓÚÑáéíóúñ\\.\\,\\s]{3,250}", ErrorMessage = "Escriba solo letras.")]
        public string ApellidoMaterno { get; set; }

        [Display(Name = "Fecha de Nacimiento *")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "Género *")]
        public string Genero { get; set; }

        [Display(Name = "Fecha de Ingreso *")]
        public DateTime FechaIngreso { get; set; }

        [Display(Name = "RFC *")]
        [Required(ErrorMessage = "El campo RFC es requerido")]
        [MinLength(11, ErrorMessage = "El RFC está incompleto")]
        public string RFC { get; set; }

        [Display(Name = "Es Activo? *")]
        public bool Activo { get; set; }

        [NotMapped]
        public string Path { get; set; }
    }
}
