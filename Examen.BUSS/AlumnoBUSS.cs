﻿using Examen.DAO;
using Examen.Models;
using System.IO;

namespace Examen.BUSS
{
    public class AlumnoBUSS 
    {
        private readonly AlumnoDAO alumnoDAO = new AlumnoDAO();

        public Estatus GuardarAlumno(Alumno alumno, string WebPath)
        {
            Estatus estatus = new Estatus
            {
                IsSuccess = false,
            };

            var pathFile = string.Empty;
            try
            {
                var pathDirectory = Path.Combine(WebPath, "Alumno");
                Directory.CreateDirectory(pathDirectory);
                pathFile = Path.Combine(WebPath, "Alumno", alumno.RFC);
            }
            catch (DirectoryNotFoundException)
            {
                estatus.Message = "Error al Crear el Directorio";
            }

            alumno.Path = pathFile;
            estatus = alumnoDAO.GuardarAlumno(alumno);
            return estatus;
        }
    }
}
