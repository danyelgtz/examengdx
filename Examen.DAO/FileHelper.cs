﻿using CsvHelper;
using Examen.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace Examen.DAO
{
    public class FileHelper
    {
        private byte[] WriteToCSV(Alumno Alumno)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(Alumno.Path+".csv"))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                var Columns = "{0},{1},{2},{3},{4},{5},{6},{7},{8}";


                streamWriter.WriteLine(Columns,
                   "Id",
                   "Nombre",
                   "Apellido Paterno",
                   "Apellido Materno",
                   "Fecha de Nacimiento",
                   "Genero",
                   "Fecha de Ingreso",
                   "Activo",
                   "RFC"
                   );
                
                    csvWriter.WriteField(Alumno.Id);

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.Nombre) ? "" : Alumno.Nombre);

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.ApellidoPaterno) ? "" : Alumno.ApellidoPaterno);

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.ApellidoMaterno) ? "" : Alumno.ApellidoMaterno);

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.FechaNacimiento.ToShortDateString()) ? "" : Alumno.FechaNacimiento.ToString("dd/MMM/yyyy"));

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.Genero) ? "" : Alumno.Genero);

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.FechaIngreso.ToShortDateString()) ? "" : Alumno.FechaIngreso.ToString("dd/MMM/yyyy"));

                    csvWriter.WriteField(
                        Alumno.Activo == null ? "" : Alumno.Activo.ToString());

                    csvWriter.WriteField(
                        String.IsNullOrEmpty(Alumno.RFC) ? "" : Alumno.RFC);

                    csvWriter.NextRecord();
            
                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }
        
        public void AlmacenarArchivo(object file)
        {

        }


        public void AlmacenarRegistro(Alumno Alumno)
        {
            var result = WriteToCSV(Alumno);
        }
    }
}
