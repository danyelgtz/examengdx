﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Examen.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebServiceController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return View();
        }

        [HttpPost]
        public bool Post()
        {
            //Requisición de Objetos Formulario
            string RFC = Request.Form["rfc"];
            var File = Request.Form.Files.GetFile("CFDI");

            //Inicialización de Variables
            string RFCEmisorCfdi = string.Empty;
            string VersionCfdi = string.Empty;
            double SubTotalCalculado=0;
            double SubtotalCfdi = 0;

            //Lectura de Archivo CFDI
            XmlReader xmlReader = XmlReader.Create(File.OpenReadStream());
            while (xmlReader.Read())
            {
                if (xmlReader.Name == "cfdi:Comprobante")
                {
                    if (xmlReader.HasAttributes)
                    {
                        VersionCfdi = xmlReader.GetAttribute("Version");
                        SubtotalCfdi = double.Parse(xmlReader.GetAttribute("SubTotal"));
                    }
                }
                if (xmlReader.Name == "cfdi:Emisor")
                {
                    if (xmlReader.HasAttributes)
                        RFCEmisorCfdi = xmlReader.GetAttribute("Rfc");
                }
                if (xmlReader.Name == "cfdi:Concepto")
                {
                    if (xmlReader.HasAttributes)
                    {
                        double importe = double.Parse(xmlReader.GetAttribute("Importe"));
                        SubTotalCalculado += importe;
                    }
                }
            }
            //Cierre de Archivo
            xmlReader.Close();
            //Validación de Negocio
            if(RFC.Equals(RFCEmisorCfdi) && VersionCfdi.Equals("3.3") && (SubTotalCalculado.Equals(SubtotalCfdi)))
            {
                return true;
            }
            else
            {
                return false;
            }


            


        }
    }
    
}