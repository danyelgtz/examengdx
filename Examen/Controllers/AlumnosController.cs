﻿using Examen.BUSS;
using Examen.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Examen.API.Controllers
{
    public class AlumnosController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly List<string> Generos = new List<string>();

        public AlumnosController(IHostingEnvironment env)
        {
            _env = env;
            Generos.Add("Masculino");
            Generos.Add("Femenino");
        }
        public IActionResult Consultar()
        {
            List<Alumno> ListaAlumnos = new List<Alumno>();
            return View(ListaAlumnos);
        }

        [HttpGet]
        public IActionResult Registrar()
        {
            ViewData["RegistroExitoso"] = false;
            ViewData["Generos"] = new SelectList(Generos);
            return View();

        }

        [HttpPost]
        public IActionResult Registrar(Alumno alumno)
        {
            AlumnoBUSS alumnoBuss = new AlumnoBUSS();
            Estatus estatus = alumnoBuss.GuardarAlumno(alumno, _env.WebRootPath);
            if (estatus.IsSuccess)
            {
                ViewData["RegistroExitoso"] = true;
            }
            return View();
        }

    }
}